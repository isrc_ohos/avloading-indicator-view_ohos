# AnimatorValueLoadingIndicatorView_ohos

**本项目是基于开源项目AnimatorValueLoadingIndicatorView进行鸿蒙化的移植和开发的，通过项目标签以及github地址（ https://github.com/81813780/AVLoadingIndicatorView ）追踪到原安卓项目版本,该项目的讲解介绍已在社区发布，可以通过网址（ https://harmonyos.51cto.com/posts/2928 ）访问相关内容。**

#### 项目介绍

项目名称：开源加载动画效果库

所属系列：鸿蒙的第三方组件适配移植

功能：支持加载动画的开关和隐藏，支持多种加载动画效果

项目移植状态：80%

调用差异：现在只能通过java代码来实现,具体的调用方式见下方实例。

开发版本：DevEco Studio 3.0 Beta2, SDK 5.0

项目发起作者：刘磊

邮箱：792301176@qq.com



- ![Alt text](https://ftp.bmp.ovh/imgs/2021/03/f459a5c956be3047.png "sample")


#### 项目介绍

编程语言：Java


#### 安装教程

在sdk5，DevEco Studio2.1 beta3下项目可直接运行

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
- ![输入图片说明](https://images.gitee.com/uploads/images/2021/0322/175045_adf905d2_8496150.png "屏幕截图.png")




#### 使用说明

布局文件定义，提供控件：BallClipRotateIndicator
引入方法：`import com.wang.avi.indicators.BallClipRotateIndicator;`
对每一个动画赋属性值，以BallClipRotateIndicator为例：
```
LayoutConfig BallClipRotateIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        BallClipRotateIndicatorConfig.setMargins(400,0,0,0);
        BallClipRotateIndicator ballClipRotateIndicator=new BallClipRotateIndicator(this);
        ballClipRotateIndicator.setLayoutConfig(BallClipRotateIndicatorConfig);
        ballClipRotateIndicator.setHeight(200);
        ballClipRotateIndicator.setWidth(200);
        ballClipRotateIndicator.setBackground(ommonElement);
        myLayout.addComponent(ballClipRotateIndicator);
```


手动控制动画开关和隐藏，四个按钮：

//启动
```
LayoutConfig startConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  start= new Button(this);
        startConfig.setMargins(300,1600,0,0);
        start.setLayoutConfig(startConfig);
        start.setText("start");
        start.setTextSize(50);
        start.setWidth(200);
        start.setHeight(150);
        start.setTextAlignment(TextAlignment.CENTER);
        ShapeElement startShape = new ShapeElement();
        startShape.setRgbColor(new RgbColor(255, 255, 255));
        start.setBackground(startShape);
        myLayout.addComponent(start);
```


//停止
```
LayoutConfig endConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  end= new Button(this);
        endConfig.setMargins(700,1600,0,0);
        end.setLayoutConfig(endConfig);
        end.setText("end");
        end.setTextSize(50);
        end.setWidth(200);
        end.setHeight(150);
        end.setTextAlignment(TextAlignment.CENTER);
        ShapeElement endShape = new ShapeElement();
        endShape.setRgbColor(new RgbColor(255, 255, 255));
        end.setBackground(endShape);
        myLayout.addComponent(end);
```


//显示：
```
LayoutConfig showConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  show= new Button(this);
        showConfig.setMargins(300,1800,0,0);
        show.setLayoutConfig(showConfig);
        show.setText("show");
        show.setTextSize(50);
        show.setWidth(200);
        show.setHeight(150);
        show.setTextAlignment(TextAlignment.CENTER);
        ShapeElement showShape = new ShapeElement();
        showShape.setRgbColor(new RgbColor(255, 255, 255));
        show.setBackground(showShape);
        myLayout.addComponent(show);
```


//隐藏：

  ```
LayoutConfig hideConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  hide= new Button(this);
        hideConfig.setMargins(700,1800,0,0);
        hide.setLayoutConfig(hideConfig);
        hide.setText("hide");
        hide.setTextSize(50);
        hide.setWidth(200);
        hide.setHeight(150);
        hide.setTextAlignment(TextAlignment.CENTER);
        ShapeElement hideShape = new ShapeElement();
        hideShape.setRgbColor(new RgbColor(255, 255, 255));
        hide.setBackground(hideShape);
        myLayout.addComponent(hide);
  ```


        start.setClickedListener(component -> ballClipRotateIndicator.start());
        end.setClickedListener(component -> ballClipRotateIndicator.stop());
        show.setClickedListener(component -> ballClipRotateIndicator.show());
        hide.setClickedListener(component -> ballClipRotateIndicator.hide());

目前已经实现的加载动画功能如下：
```
Row 1
BallPulseIndicator，BallGridPulseIndicator，BallClipRotateIndicator，BallClipRotatePulseIndicator
Row 2
      PacmanIndicator，BallClipRotateMultipleIndicator，
      SemiCircleSpinIndicator，BallRotateIndicator
Row 3
      BallScaleIndicator，LineScaleIndicator，LineScalePartyIndicator，BallScaleMultipleIndicator
Row 4
      BallPulseSyncIndicator，BallBeatIndicator，LineScalePulseOutIndicator，LineScalePulseOutRapidIndicator
Row 5
      BallScaleRippleIndicator，BallScaleRippleMultipleIndicator，BallSpinFadeLoaderIndicator，LineSpinFadeLoaderIndicator
Row 6
      BallGridBeatIndicator
Row 7
      CubeTransitionIndicator，BallZigZagIndicator，BallZigZagDeflectIndicator，BallTrianglePathIndicator
```

后续会将剩余的加载动画也移植完成。
#### 版本迭代
v0.1.0-alpha
#### 版权和许可信息
AVLoadingIndicatorView_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。
