/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.huawei.mytestproject.slice;

import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.indicators.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.DirectionalLayout.LayoutConfig;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.TextAlignment;
import ohos.bundle.AbilityInfo;

import java.util.ArrayList;

public class MainAbilitySlice extends AbilitySlice {

    private DependentLayout myLayout = new DependentLayout(this);
    private ArrayList<AVLoadingIndicatorView> animatorList=new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);
        LayoutConfig config = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        myLayout.setLayoutConfig(config);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(231,87,100));
        myLayout.setBackground(element);

        ShapeElement commonElement = new ShapeElement();
        commonElement.setRgbColor(new RgbColor(231,87,100));


        //first row
        LayoutConfig ballPulseIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        BallPulseIndicator ballPulseIndicator=new BallPulseIndicator(this);
        ballPulseIndicator.setLayoutConfig(ballPulseIndicatorConfig);
        ballPulseIndicator.setHeight(250);
        ballPulseIndicator.setWidth(250);
        ballPulseIndicator.setBackground(commonElement);
        myLayout.addComponent(ballPulseIndicator);
        animatorList.add(ballPulseIndicator);

        LayoutConfig ballGridPulseIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballGridPulseIndicatorConfig.setMargins(250,0,0,0);
        BallGridPulseIndicator ballGridPulseIndicator=new BallGridPulseIndicator(this);
        ballGridPulseIndicator.setLayoutConfig(ballGridPulseIndicatorConfig);
        ballGridPulseIndicator.setHeight(250);
        ballGridPulseIndicator.setWidth(250);
        ballGridPulseIndicator.setBackground(commonElement);
        myLayout.addComponent(ballGridPulseIndicator);
        animatorList.add(ballGridPulseIndicator);

        LayoutConfig BallClipRotateIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        BallClipRotateIndicatorConfig.setMargins(500,0,0,0);
        BallClipRotateIndicator ballClipRotateIndicator=new BallClipRotateIndicator(this);
        ballClipRotateIndicator.setLayoutConfig(BallClipRotateIndicatorConfig);
        ballClipRotateIndicator.setHeight(250);
        ballClipRotateIndicator.setWidth(250);
        ballClipRotateIndicator.setBackground(commonElement);
        myLayout.addComponent(ballClipRotateIndicator);
        animatorList.add(ballClipRotateIndicator);

        LayoutConfig BallClipRotatePulseIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        BallClipRotatePulseIndicatorConfig.setMargins(750,0,0,0);
        BallClipRotatePulseIndicator ballClipRotatePulseIndicator=new BallClipRotatePulseIndicator(this);
        ballClipRotatePulseIndicator.setLayoutConfig(BallClipRotatePulseIndicatorConfig);
        ballClipRotatePulseIndicator.setHeight(250);
        ballClipRotatePulseIndicator.setWidth(250);
        ballClipRotatePulseIndicator.setBackground(commonElement);
        myLayout.addComponent(ballClipRotatePulseIndicator);
        animatorList.add(ballClipRotatePulseIndicator);

        //two row
        LayoutConfig pacmanIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        pacmanIndicatorConfig.setMargins(0,250,0,0);
        PacmanIndicator pacmanIndicator=new PacmanIndicator(this);
        pacmanIndicator.setLayoutConfig(pacmanIndicatorConfig);
        pacmanIndicator.setHeight(250);
        pacmanIndicator.setWidth(250);
        pacmanIndicator.setBackground(commonElement);
        myLayout.addComponent(pacmanIndicator);
        animatorList.add(pacmanIndicator);

        LayoutConfig BallClipRotateMultipleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        BallClipRotateMultipleIndicatorConfig.setMargins(250,250,0,0);
        BallClipRotateMultipleIndicator ballClipRotateMultipleIndicator=new BallClipRotateMultipleIndicator(this);
        ballClipRotateMultipleIndicator.setLayoutConfig(BallClipRotateMultipleIndicatorConfig);
        ballClipRotateMultipleIndicator.setHeight(250);
        ballClipRotateMultipleIndicator.setWidth(250);
        ballClipRotateMultipleIndicator.setBackground(commonElement);
        myLayout.addComponent(ballClipRotateMultipleIndicator);
        animatorList.add(ballClipRotateMultipleIndicator);

        LayoutConfig semiCircleSpinIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        semiCircleSpinIndicatorConfig.setMargins(500,250,0,0);
        SemiCircleSpinIndicator semiCircleSpinIndicator=new SemiCircleSpinIndicator(this);
        semiCircleSpinIndicator.setLayoutConfig(semiCircleSpinIndicatorConfig);
        semiCircleSpinIndicator.setHeight(250);
        semiCircleSpinIndicator.setWidth(250);
        semiCircleSpinIndicator.setBackground(commonElement);
        myLayout.addComponent(semiCircleSpinIndicator);
        animatorList.add(semiCircleSpinIndicator);

        LayoutConfig ballRotateIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballRotateIndicatorConfig.setMargins(750,250,0,0);
        BallRotateIndicator ballRotateIndicator=new BallRotateIndicator(this);
        ballRotateIndicator.setLayoutConfig(ballRotateIndicatorConfig);
        ballRotateIndicator.setHeight(250);
        ballRotateIndicator.setWidth(250);
        ballRotateIndicator.setBackground(commonElement);
        myLayout.addComponent(ballRotateIndicator);
        animatorList.add(ballRotateIndicator);

        //three row
        LayoutConfig ballScaleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballScaleIndicatorConfig.setMargins(0,500,0,0);
        BallScaleIndicator ballScaleIndicator=new BallScaleIndicator(this);

        ballScaleIndicator.setLayoutConfig(ballScaleIndicatorConfig);
        ballScaleIndicator.setHeight(250);
        ballScaleIndicator.setWidth(250);
        ballScaleIndicator.setBackground(commonElement);
        myLayout.addComponent(ballScaleIndicator);
        animatorList.add(ballScaleIndicator);

        LayoutConfig lineScaleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        lineScaleIndicatorConfig.setMargins(250,500,0,0);
        LineScaleIndicator lineScaleIndicator=new LineScaleIndicator(this);
        lineScaleIndicator.setLayoutConfig(lineScaleIndicatorConfig);
        lineScaleIndicator.setHeight(250);
        lineScaleIndicator.setWidth(250);
        lineScaleIndicator.setBackground(commonElement);
        myLayout.addComponent(lineScaleIndicator);
        animatorList.add(lineScaleIndicator);

        LayoutConfig lineScalePartyIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        lineScalePartyIndicatorConfig.setMargins(500,500,0,0);
        LineScalePartyIndicator lineScalePartyIndicator=new LineScalePartyIndicator(this);
        lineScalePartyIndicator.setLayoutConfig(lineScalePartyIndicatorConfig);
        lineScalePartyIndicator.setHeight(250);
        lineScalePartyIndicator.setWidth(250);
        lineScalePartyIndicator.setBackground(commonElement);
        myLayout.addComponent(lineScalePartyIndicator);
        animatorList.add(lineScalePartyIndicator);

        LayoutConfig ballScaleMultipleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballScaleMultipleIndicatorConfig.setMargins(750,500,0,0);
        BallScaleMultipleIndicator ballScaleMultipleIndicator=new BallScaleMultipleIndicator(this);
        ballScaleMultipleIndicator.setLayoutConfig(ballScaleMultipleIndicatorConfig);
        ballScaleMultipleIndicator.setHeight(250);
        ballScaleMultipleIndicator.setWidth(250);
        ballScaleMultipleIndicator.setBackground(commonElement);
        myLayout.addComponent(ballScaleMultipleIndicator);
        animatorList.add(ballScaleMultipleIndicator);

        //four row
        LayoutConfig ballPulseSyncIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballPulseSyncIndicatorConfig.setMargins(0,750,0,0);
        BallPulseSyncIndicator ballPulseSyncIndicator=new BallPulseSyncIndicator(this);
        ballPulseSyncIndicator.setLayoutConfig(ballPulseSyncIndicatorConfig);
        ballPulseSyncIndicator.setHeight(250);
        ballPulseSyncIndicator.setWidth(250);
        ballPulseSyncIndicator.setBackground(commonElement);
        myLayout.addComponent(ballPulseSyncIndicator);
        animatorList.add(ballPulseSyncIndicator);

        LayoutConfig beatIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        beatIndicatorConfig.setMargins(250,750,0,0);
        BallBeatIndicator beatIndicator=new BallBeatIndicator(this);
        beatIndicator.setLayoutConfig(beatIndicatorConfig);
        beatIndicator.setHeight(250);
        beatIndicator.setWidth(250);
        beatIndicator.setBackground(commonElement);
        myLayout.addComponent(beatIndicator);
        animatorList.add(beatIndicator);

        LayoutConfig lineScalePulseOutIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        lineScalePulseOutIndicatorConfig.setMargins(500,750,0,0);
        LineScalePulseOutIndicator lineScalePulseOutIndicator=new LineScalePulseOutIndicator(this);
        lineScalePulseOutIndicator.setLayoutConfig(lineScalePulseOutIndicatorConfig);
        lineScalePulseOutIndicator.setHeight(250);
        lineScalePulseOutIndicator.setWidth(250);
        lineScalePulseOutIndicator.setBackground(commonElement);
        myLayout.addComponent(lineScalePulseOutIndicator);
        animatorList.add(lineScalePulseOutIndicator);

        LayoutConfig lineScalePulseOutRapidIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        lineScalePulseOutRapidIndicatorConfig.setMargins(750,750,0,0);
        LineScalePulseOutRapidIndicator lineScalePulseOutRapidIndicator=new LineScalePulseOutRapidIndicator(this);
        lineScalePulseOutRapidIndicator.setLayoutConfig(lineScalePulseOutRapidIndicatorConfig);
        lineScalePulseOutRapidIndicator.setHeight(250);
        lineScalePulseOutRapidIndicator.setWidth(250);
        lineScalePulseOutRapidIndicator.setBackground(commonElement);
        myLayout.addComponent(lineScalePulseOutRapidIndicator);
        animatorList.add(lineScalePulseOutRapidIndicator);

        //five row
        LayoutConfig ballScaleRippleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballScaleRippleIndicatorConfig.setMargins(0,1000,0,0);
        BallScaleRippleIndicator ballScaleRippleIndicator=new BallScaleRippleIndicator(this);
        ballScaleRippleIndicator.setLayoutConfig(ballScaleRippleIndicatorConfig);
        ballScaleRippleIndicator.setHeight(250);
        ballScaleRippleIndicator.setWidth(250);
        ballScaleRippleIndicator.setBackground(commonElement);
        myLayout.addComponent(ballScaleRippleIndicator);
        animatorList.add(ballScaleRippleIndicator);

        LayoutConfig ballScaleRippleMultipleIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballScaleRippleMultipleIndicatorConfig.setMargins(250,1000,0,0);
        BallScaleRippleMultipleIndicator ballScaleRippleMultipleIndicator=new BallScaleRippleMultipleIndicator(this);
        ballScaleRippleMultipleIndicator.setLayoutConfig(ballScaleRippleMultipleIndicatorConfig);
        ballScaleRippleMultipleIndicator.setHeight(250);
        ballScaleRippleMultipleIndicator.setWidth(250);
        ballScaleRippleMultipleIndicator.setBackground(commonElement);
        myLayout.addComponent(ballScaleRippleMultipleIndicator);
        animatorList.add(ballScaleRippleMultipleIndicator);

        LayoutConfig ballSpinFadeLoaderIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballSpinFadeLoaderIndicatorConfig.setMargins(500,1000,0,0);
        BallSpinFadeLoaderIndicator ballSpinFadeLoaderIndicator=new BallSpinFadeLoaderIndicator(this);
        ballSpinFadeLoaderIndicator.setLayoutConfig(ballSpinFadeLoaderIndicatorConfig);
        ballSpinFadeLoaderIndicator.setHeight(250);
        ballSpinFadeLoaderIndicator.setWidth(250);
        ballSpinFadeLoaderIndicator.setBackground(commonElement);
        myLayout.addComponent(ballSpinFadeLoaderIndicator);
        animatorList.add(ballSpinFadeLoaderIndicator);

        LayoutConfig lineSpinFadeLoaderIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        lineSpinFadeLoaderIndicatorConfig.setMargins(750,1000,0,0);
        LineSpinFadeLoaderIndicator lineSpinFadeLoaderIndicator=new LineSpinFadeLoaderIndicator(this);
        lineSpinFadeLoaderIndicator.setLayoutConfig(lineSpinFadeLoaderIndicatorConfig);
        lineSpinFadeLoaderIndicator.setHeight(250);
        lineSpinFadeLoaderIndicator.setWidth(250);
        lineSpinFadeLoaderIndicator.setBackground(commonElement);
        myLayout.addComponent(lineSpinFadeLoaderIndicator);
        animatorList.add(lineSpinFadeLoaderIndicator);

        //six row
        LayoutConfig ballGridBeatIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballGridBeatIndicatorConfig.setMargins(0,1250,0,0);
        BallGridBeatIndicator ballGridBeatIndicator=new BallGridBeatIndicator(this);
        ballGridBeatIndicator.setLayoutConfig(ballGridBeatIndicatorConfig);
        ballGridBeatIndicator.setHeight(250);
        ballGridBeatIndicator.setWidth(250);
        ballGridBeatIndicator.setBackground(commonElement);
        myLayout.addComponent(ballGridBeatIndicator);
        animatorList.add(ballGridBeatIndicator);

        LayoutConfig squareSpinIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        squareSpinIndicatorConfig.setMargins(250,1250,0,0);
        SquareSpinIndicator  squareSpinIndicator=new SquareSpinIndicator(this);
        squareSpinIndicator.setLayoutConfig(squareSpinIndicatorConfig);
        squareSpinIndicator.setHeight(250);
        squareSpinIndicator.setWidth(250);
        squareSpinIndicator.setBackground(commonElement);
        myLayout.addComponent(squareSpinIndicator);
        animatorList.add(squareSpinIndicator);

        LayoutConfig ballPulseRiseIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballPulseRiseIndicatorConfig.setMargins(500,1250,0,0);
        BallPulseRiseIndicator ballPulseRiseIndicator=new BallPulseRiseIndicator(this);
        ballPulseRiseIndicator.setLayoutConfig(ballPulseRiseIndicatorConfig);
        ballPulseRiseIndicator.setHeight(250);
        ballPulseRiseIndicator.setWidth(250);
        ballPulseRiseIndicator.setBackground(commonElement);
        myLayout.addComponent(ballPulseRiseIndicator);
        animatorList.add(ballPulseRiseIndicator);

        LayoutConfig triangleSkewSpinIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        triangleSkewSpinIndicatorConfig.setMargins(750,1250,0,0);
        TriangleSkewSpinIndicator triangleSkewSpinIndicator=new TriangleSkewSpinIndicator(this);
        triangleSkewSpinIndicator.setLayoutConfig(triangleSkewSpinIndicatorConfig);
        triangleSkewSpinIndicator.setHeight(250);
        triangleSkewSpinIndicator.setWidth(250);
        triangleSkewSpinIndicator.setBackground(commonElement);
        myLayout.addComponent(triangleSkewSpinIndicator);
        animatorList.add(triangleSkewSpinIndicator);

        //seven row
        LayoutConfig cubeTransitionIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        cubeTransitionIndicatorConfig.setMargins(0,1500,0,0);
        CubeTransitionIndicator cubeTransitionIndicator=new CubeTransitionIndicator(this);
        cubeTransitionIndicator.setLayoutConfig(cubeTransitionIndicatorConfig);
        cubeTransitionIndicator.setHeight(250);
        cubeTransitionIndicator.setWidth(250);
        cubeTransitionIndicator.setBackground(commonElement);
        myLayout.addComponent(cubeTransitionIndicator);
        animatorList.add(cubeTransitionIndicator);

        LayoutConfig ballZigZagIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballZigZagIndicatorConfig.setMargins(250,1500,0,0);
        BallZigZagIndicator ballZigZagIndicator=new BallZigZagIndicator(this);
        ballZigZagIndicator.setLayoutConfig(ballZigZagIndicatorConfig);
        ballZigZagIndicator.setHeight(250);
        ballZigZagIndicator.setWidth(250);
        ballZigZagIndicator.setBackground(commonElement);
        myLayout.addComponent(ballZigZagIndicator);
        animatorList.add(ballZigZagIndicator);
        LayoutConfig ballZigZagDeflectIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballZigZagDeflectIndicatorConfig.setMargins(500,1500,0,0);
        BallZigZagDeflectIndicator ballZigZagDeflectIndicator=new BallZigZagDeflectIndicator(this);
        ballZigZagDeflectIndicator.setLayoutConfig(ballZigZagDeflectIndicatorConfig);
        ballZigZagDeflectIndicator.setHeight(250);
        ballZigZagDeflectIndicator.setWidth(250);
        ballZigZagDeflectIndicator.setBackground(commonElement);
        myLayout.addComponent(ballZigZagDeflectIndicator);
        animatorList.add(ballZigZagDeflectIndicator);

        LayoutConfig ballTrianglePathIndicatorConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        ballTrianglePathIndicatorConfig.setMargins(750,1500,0,0);
        BallTrianglePathIndicator ballTrianglePathIndicator=new BallTrianglePathIndicator(this);
        ballTrianglePathIndicator.setLayoutConfig(ballTrianglePathIndicatorConfig);
        ballTrianglePathIndicator.setHeight(250);
        ballTrianglePathIndicator.setWidth(250);
        ballTrianglePathIndicator.setBackground(commonElement);
        myLayout.addComponent(ballTrianglePathIndicator);
        animatorList.add(ballTrianglePathIndicator);
        /*
         * four button
         */
        LayoutConfig startBtnConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  startBtn= new Button(this);
        startBtnConfig.setMargins(50,1750,0,0);
        startBtn.setLayoutConfig(startBtnConfig);
        startBtn.setText("start");
        startBtn.setTextSize(50);
        startBtn.setWidth(200);
        startBtn.setHeight(150);
        startBtn.setTextAlignment(TextAlignment.CENTER);
        ShapeElement startBtnShape = new ShapeElement();
        startBtnShape.setRgbColor(new RgbColor(255, 255, 255));
        startBtn.setBackground(startBtnShape);
        myLayout.addComponent(startBtn);

        LayoutConfig endBtnConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  endBtn= new Button(this);
        endBtnConfig.setMargins(300,1750,0,0);
        endBtn.setLayoutConfig(endBtnConfig);
        endBtn.setText("end");
        endBtn.setTextSize(50);
        endBtn.setWidth(200);
        endBtn.setHeight(150);
        endBtn.setTextAlignment(TextAlignment.CENTER);
        ShapeElement endBtnShape = new ShapeElement();
        endBtnShape.setRgbColor(new RgbColor(255, 255, 255));
        endBtn.setBackground(endBtnShape);
        myLayout.addComponent(endBtn);

        LayoutConfig showConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  showBtn= new Button(this);
        showConfig.setMargins(550,1750,0,0);
        showBtn.setLayoutConfig(showConfig);
        showBtn.setText("show");
        showBtn.setTextSize(50);
        showBtn.setWidth(200);
        showBtn.setHeight(150);
        showBtn.setTextAlignment(TextAlignment.CENTER);
        ShapeElement showShape = new ShapeElement();
        showShape.setRgbColor(new RgbColor(255, 255, 255));
        showBtn.setBackground(showShape);
        myLayout.addComponent(showBtn);

        LayoutConfig hideConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        Button  hideBtn= new Button(this);
        hideConfig.setMargins(800,1750,0,0);
        hideBtn.setLayoutConfig(hideConfig);
        hideBtn.setText("hide");
        hideBtn.setTextSize(50);
        hideBtn.setWidth(200);
        hideBtn.setHeight(150);
        hideBtn.setTextAlignment(TextAlignment.CENTER);
        ShapeElement hideShape = new ShapeElement();
        hideShape.setRgbColor(new RgbColor(255, 255, 255));
        hideBtn.setBackground(hideShape);
        myLayout.addComponent(hideBtn);

        startBtn.setClickedListener(component -> startAllAnimator(animatorList));
        endBtn.setClickedListener(component -> stopAllAnimator(animatorList));

        showBtn.setClickedListener(component -> showAllAnimator(animatorList));
        hideBtn.setClickedListener(component -> hideAllAnimator(animatorList));

        super.setUIContent(myLayout);
    }

    private void startAllAnimator(ArrayList<AVLoadingIndicatorView> avLoadingIndicatorViews){
        for (int i = 0; i < avLoadingIndicatorViews.size(); i++) {
            avLoadingIndicatorViews.get(i).start();
        }
    }

    private void stopAllAnimator(ArrayList<AVLoadingIndicatorView> avLoadingIndicatorViews){
        for (int i = 0; i < avLoadingIndicatorViews.size(); i++) {
            avLoadingIndicatorViews.get(i).stop();
        }
    }

    private void showAllAnimator(ArrayList<AVLoadingIndicatorView> avLoadingIndicatorViews){
        for (int i = 0; i < avLoadingIndicatorViews.size(); i++) {
            avLoadingIndicatorViews.get(i).show();
        }
    }

    private void hideAllAnimator(ArrayList<AVLoadingIndicatorView> avLoadingIndicatorViews){
        for (int i = 0; i < avLoadingIndicatorViews.size(); i++) {
            avLoadingIndicatorViews.get(i).hide();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        startAllAnimator(animatorList);
    }

    @Override
    public void onInactive() {
        super.onInactive();
        stopAllAnimator(animatorList);
    }


    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
