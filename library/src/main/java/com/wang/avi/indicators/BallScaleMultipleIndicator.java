/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/19.
 */
public class BallScaleMultipleIndicator extends AVLoadingIndicatorView {

    float[] scaleFloats=new float[]{1,1,1};
    float[] alphaInts=new float[]{255,255,255};

    public BallScaleMultipleIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        float circleSpacing=4;
        for (int i = 0; i < 3; i++) {
            paint.setAlpha(alphaInts[i]);
            canvas.scale(scaleFloats[i],scaleFloats[i],(float)getWidth()/2,(float)getHeight()/2);
            canvas.drawCircle((float)getWidth()/2,(float)getHeight()/2,(float) getWidth()/2-circleSpacing,paint);
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        long[] delays=new long[]{0, 200, 400};
        for (int i = 0; i < 3; i++) {
            final int index=i;
            AnimatorValue scaleAnim=new AnimatorValue();
            scaleAnim.setCurveType(Animator.CurveType.LINEAR);
            scaleAnim.setDuration(1000);
            scaleAnim.setLoopedCount(-1);
            addUpdateListener(scaleAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    scaleFloats[index] = v;
                    invalidate();
                }
            });
            scaleAnim.setDelay(delays[i]);

            AnimatorValue alphaAnim=new AnimatorValue();
            alphaAnim.setCurveType(Animator.CurveType.LINEAR);
            alphaAnim.setDuration(1000);
            alphaAnim.setLoopedCount(-1);
            addUpdateListener(alphaAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    alphaInts[index] =  1-v;
                    invalidate();
                }
            });
            scaleAnim.setDelay(delays[i]);

            animators.add(scaleAnim);
            animators.add(alphaAnim);
        }
        return animators;
    }

}
