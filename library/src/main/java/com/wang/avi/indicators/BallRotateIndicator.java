/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Matrix;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/17.
 */
public class BallRotateIndicator extends AVLoadingIndicatorView {

    float scaleFloat=0.5f;

    float degress;

    private Matrix mMatrix;

    public BallRotateIndicator(Context context) {
        super(context);
        mMatrix=new Matrix();
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.rotate(degress,getWidth()/2,getHeight()/2);
        float radius=getWidth()/10;
        float x = getWidth()/ 2;
        float y=getHeight()/2;

        /*mMatrix.preTranslate(-centerX(), -centerY());
        mMatrix.preRotate(degress,centerX(),centerY());
        mMatrix.postTranslate(centerX(), centerY());
        canvas.concat(mMatrix);*/



        canvas.save();
        canvas.translate(x - radius * 2 - radius, y);
        canvas.scale(scaleFloat, scaleFloat);
        canvas.drawCircle(0, 0, radius, paint);
        canvas.restore();

        canvas.save();
        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);
        canvas.drawCircle(0, 0, radius, paint);
        canvas.restore();

        canvas.save();
        canvas.translate(x + radius * 2 + radius, y);
        canvas.scale(scaleFloat, scaleFloat);
        canvas.drawCircle(0,0,radius, paint);
        canvas.restore();
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue scaleAnim=new AnimatorValue();
        scaleAnim.setDuration(1000);
        scaleAnim.setLoopedCount(-1);
        addUpdateListener(scaleAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if(v<0.5f)
                    scaleFloat = (float) (v+0.5);
                else
                    scaleFloat = (float) (-v+1.5);
                invalidate();
            }
        });

        AnimatorValue rotateAnim=new AnimatorValue();
        addUpdateListener(rotateAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degress = v*360;
                invalidate();
            }
        });
        rotateAnim.setDuration(1000);
        rotateAnim.setLoopedCount(-1);

        animators.add(scaleAnim);
        animators.add(rotateAnim);
        return animators;
    }

}
