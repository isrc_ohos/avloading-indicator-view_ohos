/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;


import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/16.
 */
public class BallClipRotateIndicator extends AVLoadingIndicatorView {

    float scaleFloat=1,degrees;

    public BallClipRotateIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.rotate(degrees,getWidth()/2,getHeight()/2);

        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(3);

        float circleSpacing=12;
        float x = getWidth()/ 2;
        float y= getHeight() / 2;
        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);

        RectFloat rectF=new RectFloat(-x+circleSpacing,-y+circleSpacing,0+x-circleSpacing,0+y-circleSpacing);
        canvas.drawArc(rectF, new Arc( -45, 270, false), paint);
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue scaleAnim=new AnimatorValue();
        scaleAnim.setDuration(750);
        scaleAnim.setLoopedCount(-1);
        addUpdateListener(scaleAnim,new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                scaleFloat = scaleHs(v);
                invalidate();
            }
        });
        AnimatorValue rotateAnim=new AnimatorValue();
        rotateAnim.setDuration(750);
        rotateAnim.setLoopedCount(-1);
        addUpdateListener(rotateAnim,new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degrees = v*360;
                invalidate();
            }
        });
        animators.add(scaleAnim);
        animators.add(rotateAnim);
        return animators;
    }

    protected float scaleHs(float v){
        float res=0;
        if (v<= (float) 1/3)
            res= (float) (-1.2*v+1);
        else if (v> (float) 1/3 && v<= (float) 2/3)
            res= (float) (-0.3*v+0.7);
        else if(v> (float) 2/3)
            res= (float) (1.5*v-0.5);
        return res;
    }

}
