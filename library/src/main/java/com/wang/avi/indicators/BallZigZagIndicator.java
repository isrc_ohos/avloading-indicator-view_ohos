/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/19.
 */
public class BallZigZagIndicator extends AVLoadingIndicatorView {

    float[] translateX=new float[2],translateY=new float[2];

    public BallZigZagIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        for (int i = 0; i < 2; i++) {
            canvas.save();
            canvas.translate(translateX[i], translateY[i]);
            canvas.drawCircle(0, 0, getWidth()/10, paint);
            canvas.restore();
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        float startX=getWidth()/6;
        float startY=getWidth()/6;
        for (int i = 0; i < 2; i++) {
            final int index=i;
            AnimatorValue translateXAnim=new AnimatorValue();
            translateXAnim.setDuration(1000);
            translateXAnim.setCurveType(Animator.CurveType.LINEAR);
            translateXAnim.setLoopedCount(-1);
            addUpdateListener(translateXAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if (index ==0){
                        translateX[index] =  translationXOne(startX,getWidth(),v);
                    }else{
                        translateX[index] =  translationXTwo(startX,getWidth(),v);
                    }
                    invalidate();
                }
            });
            AnimatorValue translateYAnim=new AnimatorValue();
            translateYAnim.setDuration(1000);
            translateYAnim.setCurveType(Animator.CurveType.LINEAR);
            translateYAnim.setLoopedCount(-1);
            addUpdateListener(translateYAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(index==0)
                        translateY[index] =  translationYHs(startY,getHeight()/2,v);
                    else
                        translateY[index] =  translationYHs(getHeight()-startY,getHeight()/2,v);
                    invalidate();
                }
            });
            animators.add(translateXAnim);
            animators.add(translateYAnim);
        }
        return animators;
    }

    protected float translationXOne(float a,float b,float v){
        float res=0;
        if (v<= (float) 1/3)
            res=3*(b-2*a)*v+a;
        else if (v> (float) 1/3 && v<= (float) 2/3)
            res=(-3*b/2+3*a)*v+3*b/2-2*a;
        else if(v> (float) 2/3)
            res=(3*a-3*b/2)*v+3*b/2-2*a;
        return res;
    }
    protected float translationXTwo(float a,float b,float v){
        float res=0;
        if (v<= (float) 1/3)
            res=3*(2*a-b)*v+b-a;
        else if (v> (float) 1/3 && v<= (float) 2/3)
            res=(3*b/2-3*a)*v+2*a-b/2;
        else if(v> (float) 2/3)
            res=(3*b/2-3*a)*v-b/2+2*a;
        return res;
    }
    protected float translationYHs(float a,float b,float v){
        float res=0;
        if (v<= (float) 1/3)
            res=a;
        else if (v> (float) 1/3 && v<= (float) 2/3)
            res=3*(b-a)*v+a*2-b;
        else if(v> (float) 2/3)
            res=3*(a-b)*v+3*b-2*a;
        return res;
    }

}
