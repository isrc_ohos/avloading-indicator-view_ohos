/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;




import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/19.
 */
public class BallZigZagDeflectIndicator extends BallZigZagIndicator {


    public BallZigZagDeflectIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        float startX=getWidth()/6;
        float startY=getWidth()/6;
        for (int i = 0; i < 2; i++) {
            final int index=i;
            AnimatorValue translateXAnim=new AnimatorValue();
            if (i==1){
                translateXAnim=new AnimatorValue();
            }
            AnimatorValue translateYAnim=new AnimatorValue();
            if (i==1){
                translateYAnim=new AnimatorValue();
            }

            translateXAnim.setDuration(2000);
            translateXAnim.setCurveType(Animator.CurveType.LINEAR);
            translateXAnim.setLoopedCount(-1);
            addUpdateListener(translateXAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if (index ==1)
                        translateX[index] =  translationXHs(getWidth()-startX,startX,v);
                    else
                        translateX[index] =  translationXHs(startX,getWidth()-startX,v);
                    invalidate();
                }
            });

            translateYAnim.setDuration(2000);
            translateYAnim.setCurveType(Animator.CurveType.LINEAR);
            translateYAnim.setLoopedCount(-1);
            addUpdateListener(translateYAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if (index ==0)
                        translateY[index] =  translationYHs(startY,getHeight()-startY,v);
                    else
                        translateY[index] =  translationYHs(getHeight()-startY,startY,v);
                    invalidate();
                }
            });

            animators.add(translateXAnim);
            animators.add(translateYAnim);
        }
        return animators;
    }

    protected float translationXHs(float a,float b,float v){
        float res=0;
        if (v<= (float) 1/4)
            res=4*(b-a)*v+a;
        else if (v> (float) 1/4 && v<= (float) 1/2)
            res=4*(a-b)*v+2*b-a;
        else if (v> (float) 1/2 && v<= (float) 3/4)
            res=4*(b-a)*v+3*a-2*b;
        else if(v> (float) 3/4)
            res=4*(a-b)*v+4*b-3*a;
        return res;
    }


    protected float translationYHs(float a,float b,float v){
        float res=0;
        if (v<= (float) 1/4)
            res=a;
        else if (v> (float) 1/4 && v<= (float) 1/2)
            res=v*4*(b-a)+(2*a-b);
        else if (v> (float) 1/2 && v<= (float) 3/4)
            res=b;
        else if(v> (float) 3/4)
            res=v*4*(a-b)+4*b-3*a;
        return res;
    }

}
