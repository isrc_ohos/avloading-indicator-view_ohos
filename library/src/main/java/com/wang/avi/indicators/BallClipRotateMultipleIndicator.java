/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;


import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/17.
 */
public class BallClipRotateMultipleIndicator extends AVLoadingIndicatorView {

    float scaleFloat=1,degrees;

    public BallClipRotateMultipleIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
        setPaint();
        setBound();
        draw(canvas,getPaint());
    };
    addDrawTask(task);
}


    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {

        canvas.rotate(degrees*360,getWidth()/2,getHeight()/2);

        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        float circleSpacing=12;
        float x=getWidth()/2;
        float y=getHeight()/2;

        canvas.save();
        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);

        //draw two big arc
        float[] bStartAngles=new float[]{135,-45};
        for (int i = 0; i < 2; i++) {
            RectFloat rectF=new RectFloat(-x+circleSpacing,-y+circleSpacing,x-circleSpacing,y-circleSpacing);

            canvas.drawArc(rectF,new Arc(bStartAngles[i], 90, false), paint);
        }

        canvas.restore();

        canvas.rotate(-degrees*720,getWidth()/2,getHeight()/2);

        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);

        //draw two small arc
        float[] sStartAngles=new float[]{225,45};
        for (int i = 0; i < 2; i++) {
            RectFloat rectF=new RectFloat(-x/1.8f+circleSpacing,-y/1.8f+circleSpacing,x/1.8f-circleSpacing,y/1.8f-circleSpacing);
            canvas.drawArc(rectF, new Arc(sStartAngles[i], 90, false), paint);
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue scaleAnim=new AnimatorValue();
        scaleAnim.setDuration(1000);
        scaleAnim.setLoopedCount(-1);
        addUpdateListener(scaleAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if(v<0.5f)
                    scaleFloat = (float) (-0.8*v+1);
                else
                    scaleFloat = (float) (0.8*v+0.2);
                invalidate();
            }
        });

        AnimatorValue rotateAnim=new AnimatorValue();
        rotateAnim.setDuration(1000);
        rotateAnim.setLoopedCount(-1);
        addUpdateListener(rotateAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degrees = v;
                invalidate();
            }
        });
        animators.add(scaleAnim);
        animators.add(rotateAnim);
        return animators;
    }

}
