/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/17.
 */
public class BallPulseRiseIndicator extends AVLoadingIndicatorView {

    private ThreeDimView mCamera;
    private Matrix mMatrix;

    private float degress;

    public BallPulseRiseIndicator(Context context){
        super(context);
        mCamera=new ThreeDimView();
        mMatrix=new Matrix();
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        mMatrix.reset();
        mCamera.rotateX(degress);
        mCamera.getMatrix(mMatrix);
        mMatrix.preTranslate(-getWidth()/2, -getHeight()/2);
        mMatrix.postTranslate(getWidth()/2, getHeight()/2);
        canvas.setMatrix(mMatrix);

        float radius=getWidth()/10;

        canvas.drawCircle(getWidth()/4,radius*2,radius,paint);
        canvas.drawCircle(getWidth()*3/4,radius*2,radius,paint);

        canvas.drawCircle(radius,getHeight()-2*radius,radius,paint);
        canvas.drawCircle(getWidth()/2,getHeight()-2*radius,radius,paint);
        canvas.drawCircle(getWidth()-radius,getHeight()-2*radius,radius,paint);
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue animator=new AnimatorValue();
        animator.setCurveType(Animator.CurveType.LINEAR);
        addUpdateListener(animator, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degress = (float) (v*36);
                invalidate();
            }
        });
        animator.setLoopedCount(-1);
        animator.setDuration(1500);
        animators.add(animator);
        return animators;
    }

}
