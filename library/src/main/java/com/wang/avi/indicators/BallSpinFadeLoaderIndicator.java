/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;







import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/20.
 */
public class BallSpinFadeLoaderIndicator extends AVLoadingIndicatorView {

    public static final float SCALE=1.0f;

    public static final float ALPHA=1.0f;

    float[] scaleFloats=new float[]{SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE};

    float[] alphas=new float[]{ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA};

    public BallSpinFadeLoaderIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        float radius=getWidth()/10;
        for (int i = 0; i < 8; i++) {
            canvas.save();
            Point point=circleAt(getWidth(),getHeight(),getWidth()/2-radius,i*(Math.PI/4));
            canvas.translate(point.x,point.y);
            canvas.scale(scaleFloats[i],scaleFloats[i]);
            paint.setAlpha(alphas[i]);
            canvas.drawCircle(0,0,radius,paint);
            canvas.restore();
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        int[] delays= {0, 120, 240, 360, 480, 600, 720, 780, 840};
        for (int i = 0; i < 8; i++) {
            final int index=i;
            AnimatorValue scaleAnim=new AnimatorValue();
            scaleAnim.setDuration(1000);
            scaleAnim.setLoopedCount(-1);
            scaleAnim.setDelay(delays[i]);
            addUpdateListener(scaleAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(v<0.5f)
                        scaleFloats[index] = (float) (-1.2*v+1);
                    else
                        scaleFloats[index] = (float) (1.2*v-0.2);
                    invalidate();
                }
            });

            AnimatorValue alphaAnim=new AnimatorValue();
            alphaAnim.setDuration(1000);
            alphaAnim.setLoopedCount(-1);
            alphaAnim.setDelay(delays[i]);
            addUpdateListener(alphaAnim, new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(v<0.5f)
                        alphas[index] = (float) (-356*v+255)/255;
                    else
                        alphas[index] = (float) (356*v-101)/255;
                    invalidate();
                }
            });
            animators.add(scaleAnim);
            animators.add(alphaAnim);
        }
        return animators;
    }

    /**
     * 圆O的圆心为(a,b),半径为R,点A与到X轴的为角α.
     *则点A的坐标为(a+R*cosα,b+R*sinα)
     * @param width
     * @param height
     * @param radius
     * @param angle
     * @return
     */
    Point circleAt(int width,int height,float radius,double angle){
        float x= (float) (width/2+radius*(Math.cos(angle)));
        float y= (float) (height/2+radius*(Math.sin(angle)));
        return new Point(x,y);
    }

    final class Point{
        public float x;
        public float y;

        public Point(float x, float y){
            this.x=x;
            this.y=y;
        }
    }


}
