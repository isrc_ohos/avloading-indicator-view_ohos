/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;



import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/16.
 */
public class BallClipRotatePulseIndicator extends AVLoadingIndicatorView {

    float scaleFloat1,scaleFloat2,degrees;

    public BallClipRotatePulseIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.rotate(degrees,getWidth()/2,getHeight()/2);

        float circleSpacing=12;
        float x=getWidth()/2;
        float y=getHeight()/2;

        //draw fill circle
        canvas.save();
        canvas.translate(x, y);
        canvas.scale(scaleFloat1, scaleFloat1);
        paint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(0, 0, x / 2.5f, paint);

        canvas.restore();
        canvas.translate(x, y);
        canvas.scale(scaleFloat2, scaleFloat2);


        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE_STYLE);

        //draw two arc
        float[] startAngles=new float[]{225,45};
        for (int i = 0; i < 2; i++) {
            RectFloat rectF=new RectFloat(-x+circleSpacing,-y+circleSpacing,x-circleSpacing,y-circleSpacing);
            canvas.drawArc(rectF, new Arc(startAngles[i], 90, false), paint);
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        AnimatorValue scaleAnim=new AnimatorValue();
        scaleAnim.setDuration(1000);
        scaleAnim.setLoopedCount(-1);
        addUpdateListener(scaleAnim,new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {

                if(v<0.5f)
                    scaleFloat1 = (float) (-1.4*v+1);
                else
                    scaleFloat1 = (float) (1.4*v-0.4);
                invalidate();
            }
        });

        AnimatorValue scaleAnim2=new AnimatorValue();
        scaleAnim2.setDuration(1000);
        scaleAnim2.setLoopedCount(-1);
        addUpdateListener(scaleAnim2, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if(v<0.5f)
                    scaleFloat2 = (float) (-0.8*v+1);
                else
                    scaleFloat2 = (float) (0.8*v+0.2);
                invalidate();
            }
        });

        AnimatorValue rotateAnim=new AnimatorValue();
        rotateAnim.setDuration(1000);
        rotateAnim.setLoopedCount(-1);
        addUpdateListener(rotateAnim,new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degrees = v*360;
                invalidate();
            }
        });
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        animators.add(scaleAnim);
        animators.add(scaleAnim2);
        animators.add(rotateAnim);
        return animators;
    }


}
