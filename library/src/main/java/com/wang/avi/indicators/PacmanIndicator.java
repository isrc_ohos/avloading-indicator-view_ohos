/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/16.
 */
public class PacmanIndicator extends AVLoadingIndicatorView {

    private float translateX;

    private float alpha;

    private float degrees1,degrees2;

    public PacmanIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }
    @Override
    public void draw(Canvas canvas, Paint paint) {
        drawCircle(canvas,paint);
        drawPacman(canvas,paint);
    }

    private void drawPacman(Canvas canvas,Paint paint){
        canvas.rotate(degrees1*60,getWidth()/2,getHeight()/2);
        float x=getWidth()/2;
        float y=getHeight()/2;

        canvas.save();

        canvas.translate(x, y);

        paint.setAlpha(1.0f);
        RectFloat rectF1=new RectFloat(-x/1.7f,-y/1.7f,x/1.7f,y/1.7f);
        canvas.drawArc(rectF1, new Arc(0, 270, true), paint);
        canvas.restore();

        canvas.rotate(-degrees2*120,getWidth()/2,getHeight()/2);
        canvas.save();
        canvas.translate(x, y);
        paint.setAlpha(1.0f);
        RectFloat rectF2=new RectFloat(-x/1.7f,-y/1.7f,x/1.7f,y/1.7f);
        canvas.drawArc(rectF2,new Arc(90,270,true),paint);
        canvas.restore();
    }


    private void drawCircle(Canvas canvas, Paint paint) {
        float radius=getWidth()/11;
        paint.setAlpha(alpha);
        canvas.drawCircle(getWidth()-radius-translateX, getHeight()/2, radius, paint);
    }



    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        float startT=getWidth()/11;
        AnimatorValue translationAnim=new AnimatorValue();
        translationAnim.setDuration(650);
        translationAnim.setCurveType(Animator.CurveType.LINEAR);
        translationAnim.setLoopedCount(-1);
        addUpdateListener(translationAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                translateX = v*4*startT;
                invalidate();
            }
        });

        AnimatorValue alphaAnim=new AnimatorValue();
        alphaAnim.setDuration(650);
        alphaAnim.setLoopedCount(-1);
        addUpdateListener(alphaAnim, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                alpha = (-123*v+255)/255;
                invalidate();
            }
        });

        AnimatorValue rotateAnim1=new AnimatorValue();
        rotateAnim1.setDuration(650);
        rotateAnim1.setLoopedCount(-1);
        addUpdateListener(rotateAnim1, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degrees1 = v;
                invalidate();
            }
        });

        AnimatorValue rotateAnim2=new AnimatorValue();
        rotateAnim2.setDuration(650);
        rotateAnim2.setLoopedCount(-1);
        addUpdateListener(rotateAnim2, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degrees2 = v;
                invalidate();
            }
        });
        animators.add(translationAnim);
        animators.add(alphaAnim);
        animators.add(rotateAnim1);
        animators.add(rotateAnim2);
        return animators;
    }
}
