/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/19.
 */
public class BallBeatIndicator extends AVLoadingIndicatorView {

    public static final float SCALE=1.0f;

    public static final float ALPHA=1.0f;

    private float[] scaleFloats=new float[]{SCALE,
            SCALE,
            SCALE};

    float[] alphas=new float[]{ALPHA,
            ALPHA,
            ALPHA,};

    public BallBeatIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        float circleSpacing=4;
        float radius=(getWidth()-circleSpacing*2)/6;
        float x = getWidth()/ 2-(radius*2+circleSpacing);
        float y=getHeight() / 2;
        for (int i = 0; i < 3; i++) {
            canvas.save();
            float translateX=x+(radius*2)*i+circleSpacing*i;
            canvas.translate(translateX, y);
            canvas.scale(scaleFloats[i], scaleFloats[i]);
            paint.setAlpha(alphas[i]);
            canvas.drawCircle(0, 0, radius, paint);
            canvas.restore();
        }
    }


    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        int[] delays=new int[]{350,0,350};
        for (int i = 0; i < 3; i++) {
            final int index=i;
            AnimatorValue scaleAnim=new AnimatorValue();
            scaleAnim.setDuration(700);
            scaleAnim.setLoopedCount(-1);
            scaleAnim.setDelay(delays[i]);
            addUpdateListener(scaleAnim,new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(v<0.5f)
                        scaleFloats[index] = (float) (-0.5*v+1);
                    else
                        scaleFloats[index] = (float) (0.5*v+0.5);
                    invalidate();
                }
            });

            AnimatorValue alphaAnim=new AnimatorValue();
            alphaAnim.setDuration(700);
            alphaAnim.setLoopedCount(-1);
            alphaAnim.setDelay(delays[i]);
            addUpdateListener(alphaAnim,new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(v<0.5f)
                        alphas[index] = (float) (-408*v+255)/255;
                    else
                        alphas[index] = (float) (408*v-153)/255;
                    invalidate();
                }
            });
            animators.add(scaleAnim);
            animators.add(alphaAnim);
        }
        return animators;
    }


}
