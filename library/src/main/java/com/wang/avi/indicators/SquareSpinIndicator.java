/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;

import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/16.
 */
public class SquareSpinIndicator extends AVLoadingIndicatorView {

    private float rotateX;
    private float rotateY;

    private ThreeDimView mCamera;
    private Matrix mMatrix;

    public SquareSpinIndicator(Context context){
        super(context);
        mCamera=new ThreeDimView();
        mMatrix=new Matrix();
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {

        mMatrix.reset();
        mCamera.rotateX(rotateX);
        mCamera.rotateY(rotateY);
        mCamera.getMatrix(mMatrix);
        mMatrix.preTranslate(-getWidth()/2, -getHeight()/2);
        mMatrix.postTranslate(getWidth()/2, getHeight()/2);
        canvas.setMatrix(mMatrix);
        canvas.drawRect(new RectFloat(getWidth()/5,getHeight()/5,getWidth()*4/5,getHeight()*4/5),paint);
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue animator=new AnimatorValue();
        addUpdateListener(animator, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                rotateX= rotateXHs(v);
                invalidate();
            }
        });
        animator.setCurveType(Animator.CurveType.LINEAR);
        animator.setLoopedCount(-1);
        animator.setDuration(2500);

        AnimatorValue animator1=new AnimatorValue();
        addUpdateListener(animator1, new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                rotateY= rotateYHs(v);
                invalidate();
            }
        });
        animator1.setCurveType(Animator.CurveType.LINEAR);
        animator1.setLoopedCount(-1);
        animator1.setDuration(2500);

        animators.add(animator);
        animators.add(animator1);
        return animators;
    }

    protected float rotateXHs(float v){
        float res=0;
        if (v<= (float) 1/4)
            res=720*v;
        else if (v> (float) 1/4 && v<= (float) 1/2)
            res=180;
        else if (v> (float) 1/2 && v<= (float) 3/4)
            res=-720*v+540;
        else
            res=0;
        return res;
    }

    protected float rotateYHs(float v){
        float res=0;
        if (v<= (float) 1/4)
            res=0;
        else if (v> (float) 1/4 && v<= (float) 1/2)
            res=720*v-180;
        else if (v> (float) 1/2 && v<= (float) 3/4)
            res=720*(1-v);
        else
            res=0;
        return res;
    }

}
