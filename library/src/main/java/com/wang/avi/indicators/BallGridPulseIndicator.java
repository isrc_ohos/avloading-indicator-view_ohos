/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;


import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/16.
 */
public class BallGridPulseIndicator extends AVLoadingIndicatorView {

    public static final float ALPHA=1.0f;

    public static final float SCALE=1.0f;

    float[] alphas=new float[]{ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA,
            ALPHA};

    float[] scaleFloats=new float[]{SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE,
            SCALE};

    public BallGridPulseIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        float circleSpacing=4;
        float radius=(getWidth()-circleSpacing*4)/6;
        float x = (float) getWidth()/ 2-(radius*2+circleSpacing);
        float y = (float) getWidth()/ 2-(radius*2+circleSpacing);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                canvas.save();
                float translateX=x+(radius*2)*j+circleSpacing*j;
                float translateY=y+(radius*2)*i+circleSpacing*i;
                canvas.translate(translateX, translateY);
                canvas.scale(scaleFloats[3 * i + j], scaleFloats[3 * i + j]);
                paint.setAlpha(alphas[3 * i + j]);
                canvas.drawCircle(0, 0, radius, paint);
                canvas.restore();
            }
        }
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        int[] durations={720, 1020, 1280, 1420, 1450, 1180, 870, 1450, 1060};
        int[] delays= {-60, 250, -170, 480, 310, 30, 460, 780, 450};

        for (int i = 0; i < 9; i++) {
            final int index=i;
            AnimatorValue scaleAnim=new AnimatorValue();
            scaleAnim.setDuration(durations[i]*5);
            scaleAnim.setCurveType(Animator.CurveType.INVALID);
            scaleAnim.setLoopedCount(-1);
            scaleAnim.setDelay(delays[i]);
            addUpdateListener(scaleAnim,new AnimatorValue.ValueUpdateListener(){
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    if(v<=0.5f)
                        scaleFloats[index] =1-v;
                    else
                        scaleFloats[index] = v;
                    invalidate();
                }
            });

            AnimatorValue alphaAnim=new AnimatorValue();
            alphaAnim.setDuration(durations[i]);
            scaleAnim.setCurveType(Animator.CurveType.INVALID);
            alphaAnim.setLoopedCount(-1);
            alphaAnim.setDelay(delays[i]);
            addUpdateListener(alphaAnim,new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    alphas[index] =hs(v);
                    invalidate();
                }
            });
            animators.add(scaleAnim);
            animators.add(alphaAnim);
        }
        return animators;
    }

    //具体的数组动画函数
    private float hs(float v){
        float res=0;
        if(v <= (float) 1/3 )
            res = (-(float)9/17)*v+1;
        else if(v> (float) 1/3 && v<=(float) 2/3)
            res = (float) (-264/255*v+298/255);
        else
            res = (float)((369/255)*v-47/255);
        return res;
    }

}
