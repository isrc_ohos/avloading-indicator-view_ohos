/*
 *    Copyright 2015 jack wang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.wang.avi.indicators;


import com.wang.avi.AVLoadingIndicatorView;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/20.
 */
public class SemiCircleSpinIndicator extends AVLoadingIndicatorView {

    private float degress;

    public SemiCircleSpinIndicator(Context context) {
        super(context);
        Component.DrawTask task = (component, canvas) -> {
            setPaint();
            setBound();
            draw(canvas,getPaint());
        };
        addDrawTask(task);
    }

    @Override
    public void addDrawTask(Component.DrawTask drawTask){
        super.addDrawTask(drawTask);
        drawTask.onDraw(this, mCanvasForTaskOverContent);
    }


    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.rotate(degress,getWidth()/2,getHeight()/2);
        RectFloat rectF=new RectFloat(0,0,getWidth(),getHeight());
        canvas.drawArc(rectF,new Arc(-60,120,false),paint);
    }

    @Override
    public ArrayList<AnimatorValue> onCreateAnimators() {
        ArrayList<AnimatorValue> animators=new ArrayList<>();
        AnimatorValue rotateAnim=new AnimatorValue();
        addUpdateListener(rotateAnim,new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                degress= v*360;
                invalidate();
            }

        });
        rotateAnim.setDuration(600);
        rotateAnim.setLoopedCount(-1);
        animators.add(rotateAnim);
        return animators;
    }

}
